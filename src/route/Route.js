import React from 'react';
import GrabFood from '../App/GrabFood';
import {createNativeStackNavigator} from '@react-navigation/native-stack'; // untuk membuat screen bisa di navigasi
import BottomNav from '../Component/BottomNav';

const Stack = createNativeStackNavigator();

function Route() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name="BottomNav" component={BottomNav} />
      <Stack.Screen name="GrabFood" component={GrabFood} />
    </Stack.Navigator>
  );
}

export default Route;

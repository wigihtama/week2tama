import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';
import icBack from '../icons/back.png';

const GrabFood = ({navigation}) => {
  return (
    <SafeAreaView style={styles.container}>
      <View
        style={{
          flexDirection: 'row',
        }}>
        <TouchableOpacity
          onPress={() => {
            navigation.goBack('Home');
          }}>
          <Image
            source={icBack}
            style={{
              width: 30,
              height: 30,
              resizeMode: 'contain',
              alignSelf: 'center',
              marginLeft: 10,
            }}
          />
        </TouchableOpacity>

        <Text
          style={
            (styles.title,
            {color: 'white', fontSize: 30, textAlign: 'center', marginLeft: 50})
          }>
          Pesanan Makanan
        </Text>
      </View>

      <View style={styles.foodContainer}>
        <Image
          style={styles.foodImage}
          source={{
            uri: 'https://w7.pngwing.com/pngs/609/629/png-transparent-thai-fried-rice-yangzhou-fried-rice-nasi-goreng-pilaf-biryani-rice-food-recipe-cuisine-thumbnail.png',
          }}
        />
        <View style={styles.foodDetails}>
          <Text style={styles.foodName}>Nasi Goreng</Text>
          <Text style={styles.foodPrice}>Rp 25.000</Text>
          <Text style={styles.foodDescription}>
            Nasi goreng pedas dengan bumbu rempah pilihan dan diolah dengan
            telur dan sayuran segar.
          </Text>
        </View>
      </View>
      <View style={styles.foodContainer}>
        <Image
          style={styles.foodImage}
          source={{
            uri: 'https://w7.pngwing.com/pngs/650/1008/png-transparent-greek-salad-caesar-salad-wrap-bean-salad-pasta-salad-salad-vegetable-salad-leaf-vegetable-food-recipe-thumbnail.png',
          }}
        />
        <View style={styles.foodDetails}>
          <Text style={styles.foodName}>Salad Sayur</Text>
          <Text style={styles.foodPrice}>Rp 20.000</Text>
          <Text style={styles.foodDescription}>
            Campuran sayuran segar seperti selada, tomat, dan timun dengan
            dressing lemon segar.
          </Text>
        </View>
      </View>
      <View style={styles.foodContainer}>
        <Image
          style={styles.foodImage}
          source={{
            uri: 'https://w7.pngwing.com/pngs/611/162/png-transparent-pasta-with-meat-balls-wonton-fish-ball-bakso-beef-ball-delicious-fish-ball-noodles-soup-food-animals-thumbnail.png',
          }}
        />
        <View style={styles.foodDetails}>
          <Text style={styles.foodName}>Bakso</Text>
          <Text style={styles.foodPrice}>Rp 17.000</Text>
          <Text style={styles.foodDescription}>
            Bakso adalah produk pangan yang terbuat dari bahan utama daging yang
            dilumatkan, dicampur dengan bahan lain, dibentuk bulatan, dan
            selanjutnya direbus.
          </Text>
        </View>
      </View>
      <View style={styles.foodContainer}>
        <Image
          style={styles.foodImage}
          source={{
            uri: 'https://w7.pngwing.com/pngs/283/962/png-transparent-gulai-nihari-gosht-rawon-gravy-menudo-soup-food-recipe-thumbnail.png',
          }}
        />
        <View style={styles.foodDetails}>
          <Text style={styles.foodName}>Rawon</Text>
          <Text style={styles.foodPrice}>Rp 28.000</Text>
          <Text style={styles.foodDescription}>
            Masakan Indonesia berasal dari Ponorogo yang berupa sup daging
            berkuah hitam dengan campuran bumbu khas yang menggunakan kluwek.
          </Text>
        </View>
      </View>

      <View style={styles.foodContainer}>
        <Image
          style={styles.foodImage}
          source={{
            uri: 'https://w7.pngwing.com/pngs/346/535/png-transparent-pizza-hut-salami-italian-cuisine-mapo-doufu-pizza-food-recipe-american-food-thumbnail.png',
          }}
        />
        <View style={styles.foodDetails}>
          <Text style={styles.foodName}>Pizza Pepperoni</Text>
          <Text style={styles.foodPrice}>Rp 35.000</Text>
          <Text style={styles.foodDescription}>
            Pizza lezat dengan saus tomat, keju mozarella, dan potongan
            pepperoni.
          </Text>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    backgroundColor: 'green',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  foodContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 10,
    padding: 10,
    borderRadius: 10,
    backgroundColor: '#f5f5f5',
    shadowOpacity: 10,
  },
  foodImage: {
    width: 100,
    height: 100,
    borderRadius: 10,
    marginRight: 10,
  },
  foodDetails: {
    flex: 1,
  },
  foodName: {
    fontSize: 18,
    fontWeight: 'bold',
  },
});

export default GrabFood;

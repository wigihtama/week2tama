import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  FlatList,
  ScrollView,
  Platform,
  Image,
} from 'react-native';
// import fonts from './assets/fonts';

const History = ({navigation}) => {
  const [item, setItem] = useState(null);

  const dataHistory = async () => {
    var myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');

    var raw = '';

    var requestOptions = {
      method: 'GET',
      headers: myHeaders,
      body: raw,
      redirect: 'follow',
    };

    fetch(
      'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/transaction.json',
      requestOptions,
    )
      .then(response => response.text())
      .then(result => {
        const val = JSON.parse(result);
        setItem(Object.entries(val));
        console.log(result);
      })
      .catch(error => console.log('error', error));
  };
  useEffect(() => {
    dataHistory();
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.headerText}>Riwayat Pembelian</Text>
      </View>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.content}>
          {item?.map((e, index) => {
            return (
              <View
                key={index}
                style={{
                  flexDirection: 'row',
                  borderWidth: 1,
                  margin: 10,
                  borderRadius: 7,
                  padding: 7,
                  backgroundColor: '#e6f2ff',
                  justifyContent: 'space-between',
                  borderColor: 'black',
                  borderBottomWidth: 5,
                  borderRightWidth: 5,
                  shadowColor: 'black',
                  shadowOffset: {width: 0, height: 2},
                  shadowOpacity: 0.9,
                  shadowRadius: 3,
                  elevation: 3,
                }}>
                <View>
                  <Text
                    style={{
                      fontFamily: 'Ubuntu-Regular',
                    }}>
                    Pengirim: {e[0]}
                  </Text>
                  <Text
                    style={{
                      fontFamily: 'Ubuntu-Regular',
                    }}>
                    Penerima: {e[1].target}
                  </Text>
                  <Text
                    style={{
                      fontFamily: 'Ubuntu-Regular',
                    }}>
                    Metode Pembayaran: {e[1].type}
                  </Text>
                  <Text
                    style={{
                      fontFamily: 'Ubuntu-Regular',
                    }}>
                    Jumlah: {e[1].amount}
                  </Text>
                </View>
                <Image
                  source={{
                    uri: 'https://img.icons8.com/dotty/512/purchase-order.png',
                  }}
                  style={{
                    width: 50,
                    height: 50,
                    resizeMode: 'contain',
                  }}
                />
              </View>
            );
          })}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#2eb82e',
  },
  box: {
    width: 0.1,
    height: 50,
    backgroundColor: 'white',
    top: 50,
    bottom: 50,
    marginBottom: 45,
  },
  header: {
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    // borderBottomWidth: 1,
    // borderBottomColor: '#ccc',
    backgroundColor: '#71da71',
    borderRadius: 50,
  },
  headerText: {
    fontSize: 18,
    // fontFamily: 'Mynerve-Regular',
    fontStyle: 'italic',
    color: 'white',
  },
});

export default History;

import React, { Component } from 'react';
import { View, TextInput, StyleSheet, Button, Text, Image } from 'react-native';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nama: '',
      phone: '',
      address: '',
      password: '',
    };
  }
  

  nama = (nama) => {
    this.setState({ nama });
  }

  phone = (phone) => {
    this.setState({ phone });
  }

  address = (address) => {
    this.setState({ address });
  }

  password = (password) => {
    this.setState({ password });
  }
  register = () => {
    const { nama, phone, address, password } = this.state;
    const data = {
      nama,
      phone,
      address,
      password
    }

    // 
    fetch('https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/users.json', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    })
    .then(response => response.json())
    .then(responseData => {
      console.log(data);
    })
    .catch(error => {
      console.error(error);
    });
  }

  render() {
    return (
      <View style={styles.container}>
         <Image
          style={styles.image}
          source={{ uri: 'https://1.bp.blogspot.com/-vNcUzj8YRPo/YNaCWN7kmLI/AAAAAAAAFaE/Q0YIFTjsM-kDUxl8VXWNHN86WZtELt8MwCLcBGAsYHQ/s1600/Logo%2BPosyandu.png' }}
        />
      <Text style={styles.header}> Silahkan Register Terlebih Dahulu</Text>
        <TextInput
          style={styles.input}
          placeholder="Kenalan dongs, Siapa Namamu"
          onChangeText={this.nama}
          value={this.state.nama}
        />
        <TextInput
          style={styles.input}
          placeholder="Minta Nomornya Dong"
          onChangeText={this.phone}
          value={this.state.phone}
        />
        <TextInput
          style={styles.input}
          placeholder="Minta Emailnya Dong"
          onChangeText={this.address}
          value={this.state.address}
        />
        <TextInput
          style={styles.input}
          placeholder="Minta Passwordnya Dong"
          secureTextEntry
          onChangeText={this.password}
          value={this.state.password}
        />
        <Button title="Register" onPress={this.register} style={styles.register}/>
        
       
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  header: {
    fontSize: 20,
    padding: 20,

  },
  input: {
    width: '80%',
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
    borderRadius: 10,
  },
  image:{
    width: 150,
    height: 130,

  },
  
});

export default App;

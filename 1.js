import {View, Text, Button} from 'react-native';
import React, {Component} from 'react';

class App extends Component {
  constructor() {
    super();
    this.state = {
      counter: 0,
    };
  }

  incrementCount() {
    this.setState({
      counter: this.state.counter + 1,
    });
  }
  decrementCount() {
    if (this.state.counter > 0) {
      this.setState(prevState => ({counter: prevState.counter - 1}));
    }
  }
  render() {
    return (
      <View>
        <Text>Count : {this.state.counter}</Text>
        <Button
          onPress={this.decrementCount.bind(this)}
          title="Kurang"></Button>
        <Button
          onPress={this.incrementCount.bind(this)}
          title="Tambah"></Button>
      </View>
    );
  }
}

export default App;